﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Milsa.MyCare.EventHub.Features.Amqp;
using System;
using System.Threading;
using System.Threading.Tasks;
using WebCrawling.Api.Features.SignalR;
using WebCrawling.Api.Features.Wykop.Articles;

namespace WebCrawling.Api.Features.Wykop.Notifications
{
    public class NotificationsService
    {
        private readonly IHubContext<MessageHub> messageHubContext;
        private readonly AmqpService amqpService;

        public NotificationsService(IHubContext<MessageHub> messageHubContext, IOptions<NotificationsServiceOptions> mongoDbServiceOptions)
        {
            this.messageHubContext = messageHubContext;
            this.amqpService = new AmqpService(mongoDbServiceOptions.Value.AmqpBrokerConnectionString);
            this.amqpService.ConnectToBroker();
        }

        public void HandleUpdateSubscription(string newSubscribedPhrase, CancellationToken cancellationToken)
        {
            this.amqpService.Publish(newSubscribedPhrase, $"{this.amqpService.TopicPrefix}OnSubscriptionPhraseUpdate");
        }

        public void ClearSearchedPhrase(CancellationToken cancellationToken)
        {
            this.amqpService.Publish("", $"{this.amqpService.TopicPrefix}OnSubscriptionPhraseUpdate");
        }

        public async Task HandleNotificationAsync(ArticleModel articleModel, CancellationToken cancellationToken)
        {
            await this.messageHubContext.Clients.All.SendAsync("NewArticleReceived", articleModel, cancellationToken);       
        }
    }
}
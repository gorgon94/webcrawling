﻿namespace WebCrawling.Api.Features.Wykop.Notifications
{
    public class NotificationsServiceOptions
    {
        public string AmqpBrokerConnectionString { get; set; }
    }
}

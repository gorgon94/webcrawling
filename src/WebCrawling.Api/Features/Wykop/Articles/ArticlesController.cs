﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WebCrawling.Api.Features.Wykop.Articles
{
    [Route("api/[controller]")]
    public class ArticlesController
    {
        private readonly ArticlesService articlesService;

        public ArticlesController(ArticlesService articlesService)
        {
            this.articlesService = articlesService;
        }

        [HttpGet]
        public async Task<ActionResult<List<ArticleModel>>> GetArticles(int pageSize, string lastRecordId, string searchedValue, CancellationToken cancellationToken)
        {
            if (pageSize <= 0) throw new Exception("Did not specified pageSize parameter");

            if (searchedValue == null)
            {
                if (lastRecordId == null)
                {
                    return await this.articlesService.GetAllArticlesFirstPage(pageSize, cancellationToken);
                }
                return await this.articlesService.GetAllArticlesNextPage(pageSize, lastRecordId, cancellationToken);
            }
            else
            {
                if (lastRecordId == null)
                {
                    return await this.articlesService.GetSearchedArticlesFirstPage(pageSize, searchedValue, cancellationToken);
                }
                return await this.articlesService.GetSearchedArticlesNextPage(pageSize, lastRecordId, searchedValue, cancellationToken);
            }
        }

    }
}
